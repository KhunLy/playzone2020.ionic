import { TestBed } from '@angular/core/testing';

import { GeolocGuard } from './geoloc.guard';

describe('GeolocGuard', () => {
  let guard: GeolocGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(GeolocGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
