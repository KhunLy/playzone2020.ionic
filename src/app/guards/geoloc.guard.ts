import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalisationService } from '../services/localisation.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeolocGuard implements CanActivate {

  constructor( private geolocService: LocalisationService ) {}
  isOk : boolean;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
    if (!this.isOk)
    {
      this.isOk = true;
      return this.geolocService.getPositions().pipe(map(c => c != null), tap())
    }
    return true;
  }
  
}
