import { Injectable } from '@angular/core';
import { Geolocation,Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { map, timeInterval } from 'rxjs/operators';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class LocalisationService {
  myLat:any;
  myLong:any;
  geoloc:Subscription;

  position$ : Observable<any>;

  constructor
  (
    private geolocation: Geolocation,
  ) { 

    this.position$ = this.geolocation.watchPosition({
      enableHighAccuracy : true,
      timeout: 500,
      maximumAge: 2000
    }).pipe(map(position => {
      if((<PositionError>position).code != null ){
        // console.log(position);
        return null;
       }
       console.log(position)
      position = (position as Geoposition);
      this.myLat = position.coords.latitude;
      this.myLong = position.coords.longitude;
      return {lat : this.myLat, long: this.myLong}
    }))
  }


  getPositions() {
    // this.storage.get('units').then((x) => {this.units = x});
    return this.position$;
  }
}
  