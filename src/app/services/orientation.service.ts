//green
import { Injectable } from '@angular/core';
import { Gyroscope, GyroscopeOrientation, GyroscopeOptions } from '@ionic-native/gyroscope/ngx'; // green work
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrientationService {

  constructor(
    private gyroscope: Gyroscope //green work
  ) { }

  getOrientationByGyroscope(): Observable<number>{

    let options: GyroscopeOptions = {
      frequency: 1000
    }

    this.gyroscope.getCurrent(options)
    .then((orientation: GyroscopeOrientation) => {
      console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
    })
    .catch();

    //calculation of angle of rotation 
    return this.gyroscope.watch()
    .pipe(map((orientation: GyroscopeOrientation) => {
        console.log(orientation.x, orientation.y, orientation.z, orientation.timestamp);
        let arrow = Math.sqrt( Math.pow(orientation.x, 2) + Math.pow(orientation.y, 2));
        return  Math.asin(orientation.y/arrow);
    }));

  }
}
